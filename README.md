Repository for uploading project 1. 

Upload a single folder for your team. The name of the folder should be ENEE719D_Fall21_lastname1_lastname2_lastname3. lastname should be replaced by last name of the team members. 

The folder should contain a readme that has the following things filled:


Team Members:

Dataset:

Tensorflow Accuracy:

Arduino Accuracy:

Size of the model:

State-of-the-art Accuracy (if any):

Latency (if not real-time):

In-addition to this the folder should contain the arduino files to compile the model, instructions to run the inference and load the data onto to the hardware. If the dataset is proprietory please say so in the readme and during the presentation.

The class of Fall 2021 had four team. They selected diverse set of datasets for training and compilation onto the arduino. 


