# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 12:17:16 2021

@author: sshah389
"""

import serial
import numpy as np
from matplotlib import pyplot
import struct

dataset = np.load("../g-test-set.npz")
X_test = dataset['X']
y_test = dataset['y']
sample = X_test[2]
sample = sample.reshape((256, 256))
i, j = 0, 0
N = 256
output = np.zeros([N, N])
input_serial = sample
ser = serial.Serial('/dev/cu.usbmodem141301', 921600, timeout=1)  ##change this to your COMport


def float_to_hex(f):
    return hex(struct.unpack('<I', struct.pack('<f', f))[0])


for i in range(N):
    for j in range(N):
        #ser.write(bytes(str(float.hex((input_serial[i, j]))), 'ascii'))

        int_value1 = int(int(((input_serial[i, j]) + 12) * 15))
        int_bytes = int_value1.to_bytes(1, 'big')
        ser.write(int_bytes)
        print("Sending the data for pixel ({}, {})".format(i, j))
pyplot.figure()
pyplot.imshow(input_serial)
pyplot.show()

for i in range(N):
    for j in range(N):
        a = ser.readline()
        output[i][j] = a
        print("Reading the data for pixel ({}, {})".format(i, j))

pyplot.figure()
pyplot.imshow(output)
pyplot.show()

ser.close()
