Team Members: Samar Sarda, Daniel Xing, Abigail Snellman
Dataset: MIT-BIH Arrhythmia Dataset, ECG Heartbeat Categorization Dataset
Tensorflow Accuracy: 94.5%
Arduino Accuracy: 95%
Size of the model: 6856 bytes
State-of-the-art Accuracy (if any): 98%
Latency (if not real-time): 0.51 seconds