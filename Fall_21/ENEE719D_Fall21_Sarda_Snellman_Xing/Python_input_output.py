# -*- coding: utf-8 -*-
"""
Created on Tue Aug 31 14:41:17 2021

@author: sshah389
"""


import serial
import numpy as np
from matplotlib import pyplot 
import math
import scipy.io as sio
import tensorflow as tf
import os
import time
import pandas as pd 
base = os.path.dirname(os.path.abspath(__file__))

i=0
try: 
    
    
   
    y_test_pred_tflite=sio.loadmat(os.path.join(base, 'tflite_pred.mat')) #This is what was predicted in you python file (Google Colab)
    y_test_pred_tflite=y_test_pred_tflite['y_test_pred_tflite']
    num_samps = np.shape(y_test_pred_tflite)[0]; 

    df_test = pd.read_csv(os.path.join(base, 'mitbih_test.csv'), header=None)

    Y_test = np.array(df_test[187].values).astype(np.int8)
    X_test = np.array(df_test[list(range(187))].values)[..., np.newaxis]
    X_test = np.reshape(X_test, [num_samps,187,1])
    
    indices = Y_test != 0; 
    Y_test = Y_test[indices]; 
    X_test = X_test[indices]; 
    length = len(Y_test)
    correct_pred = 0;
    predictions = np.zeros([length])
    for sample in range(length):
        try: 
            start_time = time.time()
            ser = serial.Serial('COM3', 9600, timeout=1) ##change this to your COMport
            #sample = 4;
       
            M=0 #start 
            N=187
            #sample=1000
            input_1=X_test[M+sample,0:N]
            int_value=np.zeros([N])
            out1=np.zeros([N,1])
            #dims_input=ser.readline()
            #dims_input1=ser.readline()
            #dims_input2=ser.readline()
            for i in range(0,N):
                int_value[i]=int(((X_test[M+sample,i]+1)*127))
                int_value1=int(int_value[i])
                int_bytes= int_value1.to_bytes(1,'big')
                ser.write(int_bytes)
                #print(ser.readline())
                out1[i] =  ser.readline()
            #
            pred =  int(ser.readline().rstrip())
            #pred  = int(ser.readline().rstrip());
            predictions[sample] = pred; 
            correct_label = Y_test[M+sample]; 
            print("printing output:")
            print('prediction:', pred)
            print('label: ', correct_label);
            pyplot.plot(input_1,'r*')#These are just to make sure the input is similar to what is recieved in the hardware
            pyplot.plot(out1)
            pyplot.show()
            
            executionTime = round((time.time() - start_time),3)
            print('Execution time in seconds: ' + str(executionTime))
            if(pred == correct_label):
                correct_pred += 1; 
            ser.close();
        except Exception as e: 
            print(e)
            ser.close(); 
            
            continue; 
    #print('out1:',pred1)#These are from the hardware you can use it to compare this with the one from the python modeling.
    #print('out2:',pred2)
    accuracy = round((correct_pred / 500)*100,2); 
    print('accuracy: ', accuracy, "%")
    #print('out1_tflite_pred:',y_test_pred_tflite[sample,0])
    #print('out2_tflite_pred:',y_test_pred_tflite[sample,1])
finally: 
    ser.close()


# pyplot.plot(input_1,'r*')#These are just to make sure the input is similar to what is recieved in the hardware
# pyplot.plot(out1)
# pyplot.show()

# print('out1:',pred1)#These are from the hardware you can use it to compare this with the one from the python modeling.
# print('out2:',pred2)


# y_test_pred_tflite=y_test_pred_tflite['y_test_pred_tflite']

# print('out1_tflite_pred:',y_test_pred_tflite[sample,0])
# print('out2_tflite_pred:',y_test_pred_tflite[sample,1])

