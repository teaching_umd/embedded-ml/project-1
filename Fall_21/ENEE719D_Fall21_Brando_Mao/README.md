Team Members: Carl Brando, Xiangyu Mao

Dataset: "EEG Brainwave Dataset: Feeling Emotions"

Tensorflow Accuracy: 
TF: 99.58% 
TFLite: 99.58%
Quantized: 99.58%

Arduino Accuracy:
97.09%

Size of the model: 
2395 params
TFLite: 12524 bytes
TFLite Quantized: 6224 bytes (-6300 bytes)

State-of-the-art Accuracy (if any):
Dense: 98%
GRU: 98.125%

Latency (if not real-time): N/A

Steps same to assignment 1 for running the inference and Aruduino
