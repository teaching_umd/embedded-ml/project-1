# -*- coding: utf-8 -*-
"""
Created on Tue Aug 31 14:41:17 2021

@author: sshah389
"""


import serial
import numpy as np
from matplotlib import pyplot
import scipy.io as sio
import tensorflow as tf
import sounddevice as sd
import json
import pandas as pd
from sklearn.model_selection import train_test_split, StratifiedKFold
from sklearn.preprocessing import LabelEncoder
from sklearn import preprocessing
import sys

i=0
ser = serial.Serial('COM20', 9600, timeout=1) ##change this to your COMport


#Decode Data
data = pd.read_csv("emotions.csv")
le = LabelEncoder()
data['label']=le.fit_transform(data['label'])

Label_data = data.pop('label')
Input = np.array(data)

Label = pd.get_dummies(Label_data)

# Extract the relevent data and normalize
Input = Input[:,545:1274]
Input = preprocessing.normalize(Input)


print("Input shape:")
print(Input.shape)
print("Label shape:")
print(Label.shape)

x_values=Input
y_values=Label

num_samples = len(y_values)

print("Num samples: {}".format(num_samples))

# Shuffel the order that the tests are run on the hardware
sample_idx = np.array(range(num_samples))
np.random.shuffle(sample_idx)

total_correct = 0
progress = 0
incorrect_samples = []


for sample in sample_idx:

    M=0
    N = len(x_values[sample])
    
    input_1=x_values[sample,0:N]
    
    #sd.play(input_1,N)
    
    int_value=np.zeros([N])
    out1=np.zeros([N])
    dims_input=ser.readline()
    dims_input1=ser.readline()
    dims_input2=ser.readline()
    
    print("dims_input: {}, dims_input1: {}, dims_input2: {}", dims_input, 
          dims_input1, dims_input2)
    
    for i in range(0,N):
        int_value[i]=int(((x_values[sample,i]+1)*127))
        int_value1=int(int_value[i])
        int_bytes= int_value1.to_bytes(1,'big')
        ser.write(int_bytes)
        
        r = ser.readline()
        
        #print("read from serial:{}".format(r))
        
        try:
            out1[i] =  r
        except:
            print(ser.readline())
            sys.exit(-1)
        
                
    pred1 =  ser.readline()
    pred2 =  ser.readline()
    pred3 =  ser.readline()
    
    
    hw_out = np.array([pred1, pred2, pred3])

    ground_truth = np.array([y_values[0][sample], y_values[1][sample], y_values[2][sample]])

    print('out1:', hw_out)
    print('ground truth:', ground_truth)
    
    # compare the output of the hw classifier with the ground truth label
    if np.argmax(hw_out) == np.argmax(ground_truth):
        total_correct += 1
    else:
        incorrect_samples.append(int(sample))
        
    print("So far {} correct out of {} ({}%)".format(total_correct,progress + 1,(total_correct * 100)/(1 + progress)))
    print("{} % complete".format(((progress + 1)/num_samples) * 100))
    
    progress += 1
    
ser.close()


print("final accuracy for hardware: {}%".format((total_correct/num_samples)*100))


report = {}

report["incorrect"] = incorrect_samples
report["accuracy:"] = float((total_correct/num_samples)*100)
report["total_ran"] = int(num_samples)
report["num_correct"] = int(total_correct)

with open('report.json','w') as jfile:
    json.dump(report, jfile)

#pyplot.plot(input_1,'r*')#These are just to make sure the input is similar to what is recieved in the hardware
#pyplot.plot(out1)
#pyplot.show()



#y_test_pred_tflite=y_test_pred_tflite['y_test_pred_tflite']



