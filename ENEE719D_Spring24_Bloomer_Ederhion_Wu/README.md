Team Members: Kyle Wu, Jefferson Ederhion, Everest Bloomer
Dataset: CIFAKE (https://www.kaggle.com/datasets/birdy654/cifake-real-and-ai-generated-synthetic-images)
Tensorflow Accuracy: 92%
Arduino Accuracy: 75%
Size of the Model: 92 KBs Parameters, 272 KBs Arduino Upload
State of the Art Accuracy: 92.98% Source Paper, 97.08% Refined Transfer Model
Latency: 0.225 Seconds

Instructions to Run Inference:
1) Upload the Arduino AI_Image to your Arduino Nano 33
2) Check through the Python_input_output.py file:
	*) Update the COM port for your Arduino
	*) Update the file locations to where you located the test repository
3) Run the Python Script