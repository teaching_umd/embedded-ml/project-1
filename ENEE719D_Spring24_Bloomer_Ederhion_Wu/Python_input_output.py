# -*- coding: utf-8 -*-
"""
ENEE719D - Project 1
Kyle Wu
Jefferson Ederhion
Everest Bloomer
"""

import serial
import numpy as np
from matplotlib import pyplot 
import math
import scipy.io as sio
import tensorflow as tf
from sklearn.metrics import accuracy_score
from PIL import Image
import os

i=0
ser = serial.Serial('COM5', 9600, timeout=1) ##change this to your COMport

# Initialize array to store hardware predictions
hardware_preds = []

# Load and label images
image_dirs = ['./archive/test/FAKE', './archive/test/REAL'] ##Change this to your dataset locations
labels = [0, 1]
x_values = []
y_values = []
count = 0

for dir, label in zip(image_dirs, labels):
    count = 0
    for filename in os.listdir(dir):
        # if count >= 10:
        #   // break
        img = Image.open(os.path.join(dir, filename))
        if img is not None:
            x_values.append(np.array(img))
            y_values.append(label)
            # count += 1

            

x_values = np.array(x_values)
y_values = np.array(y_values)

# Reshape x_values to 32x32x3 and normalize
x_values = np.reshape(x_values, [x_values.shape[0], 32, 32, 3])
x_values = (x_values - 127.5) / 127.5

# Convert y_values to categorical
y_values = tf.keras.utils.to_categorical(y_values)

N = 32*32*3 # Change to 32x32x3

#sample = 0


for sample in range(x_values.shape[0]): # Predict for all records    
    input_1 = x_values[sample, 0:N].flatten()
    int_value = np.zeros([N])
    out1 = np.zeros([N])
    ser.write(b'\x7f')
    model_correct = ser.readline()
    size = ser.readline()
    dims_input1 = ser.readline()
    dims_input2 = ser.readline()
    dims_input3 = ser.readline()
    dims_input4 = ser.readline()
    
    
    for i in range(0, N):
        int_value[i] = int(((input_1[i] + 1) * 127))
        int_value1 = int(int_value[i])
        int_bytes = int_value1.to_bytes(1, 'big')
        ser.write(int_bytes)
        out1[i] = ser.readline()
    pred1 = ser.readline()
    
    
    # Store hardware predictions
    hardware_preds.append(float(pred1))
    
    print(sample, hardware_preds[sample])
    
ser.close()

rounded_preds = np.round(hardware_preds)
# Calculate accuracy
y_binary = np.argmax(y_values, axis=1)
accuracy = accuracy_score(y_binary, rounded_preds)
print('\n\n===============================================================')
print('Accuracy of the hardware: ', accuracy)
