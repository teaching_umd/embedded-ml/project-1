import numpy as np
import serial
import numpy as np
from matplotlib import pyplot 
import math
import scipy.io as sio
import tensorflow as tf
import traceback

from sklearn.metrics import f1_score, accuracy_score

def index_of_max(arr):
    return np.argmax(arr)


y_test_pred_tflite=sio.loadmat('dataset/model/tflite_pred.mat') #This is what was predicted in you python file (Google Colab)
loaded_arr = np.load('dataset/dataset_EEG_M2_test.npz')
print("Input Shape : {} ".format(loaded_arr['input'].shape))
print("Output Shape : {} ".format(loaded_arr['labels'].shape))
print(y_test_pred_tflite)


Input_test=loaded_arr['input']
x_values=np.reshape(Input_test,[Input_test.shape[0],Input_test.shape[2]*Input_test.shape[3],1])
y_values=loaded_arr['labels']
y_preds=np.zeros(y_values.shape)

loaded_arr = np.load('predicts.npz')

print(loaded_arr['hpred1'])
for i in range(len(loaded_arr['hpred1'])):
    # print("{} {}".format(y_values[i], index_of_max([loaded_arr['hpred1'][i],
                                                                  # loaded_arr['hpred2'][i],
                                                                  # loaded_arr['hpred3'][i],
                                                                  # ]) ))
    
    
    y_preds[i] = index_of_max([loaded_arr['hpred1'][i],
                               loaded_arr['hpred2'][i],
                               loaded_arr['hpred3'][i]]) 
    
    
print("F1 : ",f1_score(y_values, y_preds, average=None))
print("Acc : ",accuracy_score(y_values, y_preds))