
import serial
import numpy as np
from matplotlib import pyplot 
import math
import scipy.io as sio
import tensorflow as tf
import traceback
i=0
ser = serial.Serial('COM18', 9600, timeout=1) ##change this to your COMport



y_test_pred_tflite=sio.loadmat('dataset/model/tflite_pred.mat') #This is what was predicted in you python file (Google Colab)
loaded_arr = np.load('dataset/dataset_EEG_M2_test.npz')
print("Input Shape : {} ".format(loaded_arr['input'].shape))
print("Output Shape : {} ".format(loaded_arr['labels'].shape))
print(y_test_pred_tflite)


Input_test=loaded_arr['input']
x_values=np.reshape(Input_test,[Input_test.shape[0],Input_test.shape[2]*Input_test.shape[3],1])
y_values=loaded_arr['labels']
y_values= tf.keras.utils.to_categorical(y_values)

def index_of_max(arr):
    return np.argmax(arr)

print("x_values.shape[0]): {}".format(x_values.shape[0]))
try:
    print("X shape {}".format(x_values.shape))
    hpred =np.zeros([x_values.shape[0]])
    hpred1 =np.zeros([x_values.shape[0]])
    hpred2 =np.zeros([x_values.shape[0]])
    hpred3 =np.zeros([x_values.shape[0]])
    
    tflite_out = np.zeros([x_values.shape[0]])
    hflile_out = np.zeros([x_values.shape[0]])
    sample=10#You can change this to test out differen samples
    M=0
    N=5120
    
    samples = [i for i in range(3)]
    samples.extend([j for j in range(x_values.shape[0]-1, x_values.shape[0]-5, -1)])
    for sample in range(x_values.shape[0]):
    # for sample in range(5):
        input_1=x_values[sample,0:N]
        int_value=np.zeros([N])
        out1 =np.zeros([N])
        ser.write(b'\x7f')
        model_correct=ser.readline()
        size=ser.readline()
        dims_input1=ser.readline()
        dims_input2=ser.readline()
        dims_input3=ser.readline()
        # dims_input4=ser.readline()
        for i in range(0,N):
            int_value[i]=int(((x_values[sample,i]+1)*127))
            int_value1=int(int_value[i])
            int_bytes= int_value1.to_bytes(1,'big')
            ser.write(int_bytes)
            out1[i] =  ser.readline()
        pred1 =  ser.readline()
        pred2 =  ser.readline()
        pred3 =  ser.readline()
        # pred4 =  ser.readline()

        print("Sample: {}".format(sample))
        print(" true: {} pred1 {} pred2 {} pred3 {}".format(index_of_max(y_values[sample]), float(pred1), float(pred2), float(pred3)))
        
        
        hpred1[sample] = float(pred1)
        hpred2[sample] = float(pred2)
        hpred3[sample] = float(pred3)
        
        #Accuracy computations
        #varaibles to keep the correct count on classification 

        

except Exception as e:
    # Print the exception message
   print("An error occurred:", e)
   # Print the traceback
   traceback.print_exc()
   
np.savez("predicts.npz", hpred1=hpred1, hpred2=hpred2, hpred3=hpred3)
ser.close()