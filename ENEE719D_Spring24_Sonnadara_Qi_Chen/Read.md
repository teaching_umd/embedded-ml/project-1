Team Members: Charana Sonnadara, Xiaowen Qi, Matthew Chen \

Dataset: EEG Motor Movement/Imagery Dataset https://physionet.org/content/eegmmidb/1.0.0/ \

Tensorflow Accuracy: 92.13% \

Arduino Accuracy: 84.97% \

Size of the model: 14152 Bytes \

State-of-the-art Accuracy (if any): \
    Inter: 83.2% \
    Intra: 98.3% \

Instrunctions to run inference\
    1) Upload the arduino code Arduino Nano BLE
    2) Run the python code python_InOut.py (Update the paths dataset_EEG_M2_test.npz)
    3) Run after that run the eval.py using the predicts.npz created in the step 2