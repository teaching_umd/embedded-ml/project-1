import serial
import numpy as np
from matplotlib import pyplot 
import math
import scipy.io as sio
import tensorflow as tf
import cv2
import os
import time
i=0
ser = serial.Serial('COM5', 9600, timeout=1) ##change this to your COMport

images = []
labels = []

for label, sub_dir in enumerate(['benign', 'malignant']):
  sub_dir_path = os.path.join('./test', sub_dir)
  for image_name in os.listdir(sub_dir_path):
    image_path = os.path.join(sub_dir_path, image_name)
    image = cv2.imread(image_path)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    images.append(image)
    labels_temp = np.zeros(2).astype(int)
    labels_temp[label] = 1
    labels.append(labels_temp)

print("dataset loaded")

images = [cv2.resize(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY), (0, 0), fx = 0.5, fy = 0.5) for img in images]

images = np.array(images)

images = np.expand_dims(images, axis=-1)

labels = np.array(labels)
indices = np.arange(images.shape[0])
np.random.shuffle(indices)
x_values = images[indices]
y_values = labels[indices]

N=12544

x_values=np.reshape(x_values,(660,N))

#for our test we used samples 0 to 10 and 390 to 400, to test on both benign and malignant

sample_start = 390
sample_end = 400

int_value=np.zeros([N])
out1=np.zeros([N])

correct = 0
offset = np.random.randint(0,600)
for sample in range(sample_start,sample_end):
    start = time.time()
    print(sample)
    ser.write(b'\x7f')
    model_correct=ser.readline()
    size=ser.readline()
    dims_input1=ser.readline()
    dims_input2=ser.readline()
    dims_input3=ser.readline()
    dims_input4=ser.readline()
    for i in range(0,N):
        int_value[i]=int((x_values[sample,i])/2)
        int_value1=int(int_value[i])
        int_bytes= int_value1.to_bytes(1,'big')
        ser.write(int_bytes)
        out1[i] =  ser.readline()
    pred1 =  ser.readline()
    pred2 =  ser.readline()
    if y_values[sample][0] == 1:
        if pred1 > pred2:
            correct = correct +1
    if y_values[sample][0] == 0:
        if pred1 < pred2:
            correct = correct +1
    end = time.time()
    
ser.close()

print("Time: ", end-start)
print ("Accuracy:  %", (correct/(sample_end-sample_start))*100)



