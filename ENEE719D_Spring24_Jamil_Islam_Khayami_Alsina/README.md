Team Members: Artur Alsina, Hossein Khayami, Muhammad Islam, Rachid Jamil
Dataset: https: //www.kaggle.com/datasets/fanconic/skin-cancer-malignant-vs-benign
Tensorflow Accuracy: 78%
Arduino Accuracy: 75%
Size of the model: 8856 Bytes
State-of-the-art Accuracy (if any): 85% - model size: (VGG , 57.8mb)
Latency (if not real-time): 8 seconds

Instructions: To deploy and run the model on the Arduino place all the files in submission directory in one folder (having the same name as the .ino file) with the test dataset from Kaggle. Run the .ino file and verify then upload the model on the Arduino. Finally run the python file “Python_input_output” to get the predictions/results based on the samples you choose (sample_start , sample_end) from the 660 samples.

